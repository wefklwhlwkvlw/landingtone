importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {
	console.log(`Yay! Workbox is loaded 🎉`);
	workbox.routing.registerRoute(
		new RegExp('https:\/\/th.r.worldssl.net\/.*\.(?:png|gif|jpg|jpeg|svg|js|css|json|webp)$'),
		new workbox.strategies.CacheFirst({
			cacheName: 'cdn',
			plugins: [
			  new workbox.expiration.Plugin({
				maxEntries: 60,
				maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
			  }),
			],
		}),
	);
} else {
  console.log(`Boo! Workbox didn't load 😬`);
}
